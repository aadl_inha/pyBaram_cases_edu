//+
Point(1) = {-0.5, 0, 0, 1.0};
//+
Point(2) = {0, 0, 0, 1.0};
//+
Point(3) = {1, -0.2679, 0, 1.0};
//+
Point(4) = {1, 1, 0, 1.0};
//+
Point(5) = {-0.5, 1, 0, 1.0};
//+
Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Line(3) = {3, 4};
//+
Line(4) = {4, 5};
//+
Line(5) = {5, 1};
//+
Curve Loop(1) = {5, 1, 2, 3, 4};
//+
Plane Surface(1) = {1};
//+
Transfinite Curve {1} = 21 Using Progression 0.95;
//+
Transfinite Curve {2} = 41 Using Progression 1.05;
//+
Transfinite Curve {5, 3} = 41 Using Progression 1;
//+
Transfinite Curve {4} = 61 Using Progression 1;
//+
Physical Curve("Inflow", 6) = {5};
//+
Physical Curve("outflow", 7) = {3};
//+
Physical Curve("sym", 8) = {1, 4};
//+
Physical Curve("wall", 9) = {2};
//+
Physical Surface("fluid", 10) = {1};

//+
d = 0.0075;
Point(1) = {0, 0, 0, d};
Point(2) = {1.5, 0, 0, d};
Point(3) = {1.5, 0.6858, 0, d};
Point(4) = {1, 0.6858, 0, d};
Point(5) = {0, 1, 0, d};
//+
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 5};
Line(5) = {5, 1};
//+
Curve Loop(1) = {5, 1, 2, 3, 4};
//+
Plane Surface(1) = {1};
//+
Physical Curve("inflow", 5) = {5};
Physical Curve("outflow", 6) = {2};
Physical Curve("wall", 7) = {3, 4, 1};
Physical Surface("fluid", 8) = {1};

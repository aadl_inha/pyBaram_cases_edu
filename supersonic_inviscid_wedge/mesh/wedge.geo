//+
d = 0.01;
Point(1) = {0, 0, 0, d};
Point(2) = {2, 0.7279, 0, d};
Point(3) = {2, 1.3973, 0, d};
Point(4) = {0, 1.75, 0, d};
//+
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
//+
Curve Loop(1) = {4, 1, 2, 3};
//+
Plane Surface(1) = {1};
//+
Physical Curve("inflow", 5) = {4};
Physical Curve("outflow", 6) = {2};
Physical Curve("wall", 7) = {3, 1};
Physical Surface("fluid", 8) = {1};

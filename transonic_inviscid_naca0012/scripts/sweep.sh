#!/bin/bash
# Please run on the parent folder

for mach in 0.2 0.4 0.6 0.7 0.8
do
  mkdir m$mach
  cd m$mach
  python ../scripts/mach_sweep.py ../naca0012.ini $mach naca0012.ini 
  pybaram run ../naca0012.pbrm naca0012.ini
  cd ..
done

Point(1) = {0, 0, 0, 0.1};
Point(2) = {1, 0.1763, 0, 0.1};
Point(3) = {1.5, 0.175, 0, 0.1};
Point(4) = {3.5, 0, 0, 0.1};
Point(5) = {4, 0, 0, 0.1};
Point(6) = {4, 0.6212, 0, 0.1};
//+
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 5};
Line(5) = {5, 6};
//+
Point(7) = {1.5, 0.6212, 0, 0.1};
Point(8) = {1, 0.6212, 0, 0.1};
Point(10) = {1.5, 1, 0, 0.1};
Point(12) = {-0.5, 0, 0, 0.1};
Point(13) = {-0.5, 1, 0, 0.1};
//+
Line(6) = {12, 1};
Line(7) = {6, 7};
Line(8) = {7, 8};
Line(9) = {12, 13};
Line(10) = {13, 10};
//+
Point(14) = {1.5, 0.65, 0, 0.1};
Line(11) = {10, 14};
Line(12) = {14, 8};
//+
Curve Loop(1) = {9, 10, 11, 12, -8, -7, -5, -4, -3, -2, -1, -6};
Plane Surface(1) = {1};
Curve Loop(2) = {6, 1, 2, 3, 4, 5, 7, 8, -12, -11, -10, -9};
//+
Transfinite Curve {6} = 21 Using Progression 1;
Transfinite Curve {1} = 41 Using Progression 1;
Transfinite Curve {2} = 21 Using Progression 1;
Transfinite Curve {8} = 21 Using Progression 1;
Transfinite Curve {12} = 21 Using Progression 1;
Transfinite Curve {3} = 51 Using Progression 1;
Transfinite Curve {4} = 16 Using Progression 1;
Transfinite Curve {7} = 61 Using Progression 1;
Transfinite Curve {5} = 21 Using Progression 1;
//+
Physical Surface("fluid", 13) = {1};
Physical Curve("inflow", 14) = {9};
Physical Curve("sym", 15) = {6};
Physical Curve("wall", 16) = {1, 2, 3, 4, 7, 8};
Physical Curve("far", 17) = {10, 11};
Physical Curve("pout", 18) = {5};
Physical Curve("wall", 16) += {12};//+
Transfinite Curve {9} = 31 Using Progression 1;
Transfinite Curve {10} = 61 Using Progression 1;
//+
Transfinite Curve {11} = 11 Using Progression 1;

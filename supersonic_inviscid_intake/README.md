초음속 공기 흡입구 비점성 유동 해석
============================================
초음속 공기 흡입구 비점성 유동 해석.


1. 격자 생성 : gmsh를 이용해서 비점성 격자를 생성
    - 이미 생성되어 있음

    - 격자 확인 또는 변형은 gmsh를 이용해서 가능
    
          user@Computer:~/$ gmsh mesh/intake.geo

2. pyBaram 양식으로 격자 변경

       user@Computer:~/$ pybaram import mesh/intake.msh intake.pbrm

3. 계산 실행

       user@Computer:~/$ pybaram run intake.pbrm intake.ini


4. VTK 형식으로 유동 해석 결과 변경

       user@Computer:~/$ pybaram export intake.pbrm out-5000.pbrs out.vtu

5. Paraview를 이용한 유동 가시화
   - 저장된 Contour state로 가시화

         user@Computer:~/$ paraview --state=results/contour.pvsm

        ![Mach Contour](./results/mach.png)

6. 유동 계수 분석
   - 저장된 ``surface_pout.csv'' 파일에 기록된 출구면에서 마하수와 유량 확인


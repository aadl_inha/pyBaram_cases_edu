공기역학 수업 관련 pyBaram 유동 해석 사례
=========================================

pyBaram
--------

- 코드는 [저장소](https://gitlab.com/aadl_inha/pyBaram) 에서 구할 수 있다.

- 설치 및 사용 방법은 [설명서](https://aadl_inha.gitlab.io/pyBaram/) 에서 확인할 수 있다.

- 간단하게 익형 주위 격자를 생성하는 방법은 [슬라이드](https://docs.google.com/presentation/d/1Bt6Ki7rbma96CuWc751xGv3GyoavS0k2-hS4Z7Q7DhA/edit?usp=sharing) 확인할 수 있다.


저속 공기 역학
---------------

- [NACA2412 익형 주위 아음속 비점성 유동 해석](https://gitlab.com/aadl_inha/pyBaram_cases_edu/-/tree/main/subsonic_inviscid_naca2412)

- [평판 주위 아음속 층류 유동 해석](https://gitlab.com/aadl_inha/pyBaram_cases_edu/-/tree/main/subsonic_laminar_flat_plate)

- [NACA2412 익형 주위 난류 아음속 유동 해석](https://gitlab.com/aadl_inha/pyBaram_cases_edu/-/tree/main/subsonic_turbulent_naca2412)

고속 공기 역학
--------------

- [NACA0012 익형 주위 천음속 비점성 유동 해석](https://gitlab.com/aadl_inha/pyBaram_cases_edu/-/tree/main/transonic_inviscid_naca0012)

- [초음속 경사충격파 유동 해석](https://gitlab.com/aadl_inha/pyBaram_cases_edu/-/tree/main/supersonic_inviscid_oblique_shock)

- [초음속 팽창파 유동 해석](https://gitlab.com/aadl_inha/pyBaram_cases_edu/-/tree/main/supersonic_inviscid_expansion)

- [초음속 충격파 상호작용 유동 해석](https://gitlab.com/aadl_inha/pyBaram_cases_edu/-/tree/main/supersonic_inviscid_wedge)

- [초음속 충격파 마하 반사 해석](https://gitlab.com/aadl_inha/pyBaram_cases_edu/-/tree/main/supersonic_inviscid_mach_reflection)

- [초음속 공기 흡입구 비점성 유동 해석](https://gitlab.com/aadl_inha/pyBaram_cases_edu/-/tree/main/supersonic_inviscid_intake)


초음속 경사 충격파 해석
============================================
$M=2.5$ 초음속 유동이 $15^{\circ}$ 에 의해 발생하는 충격파 해석


1. 격자 생성 : gmsh를 이용해서 비점성 격자를 생성
    - 이미 생성되어 있음

    - 격자 확인 또는 변형은 gmsh를 이용해서 가능
    
          user@Computer:~/$ gmsh mesh/wedge.geo

2. pyBaram 양식으로 격자 변경

       user@Computer:~/$ pybaram import mesh/wedge.msh wedge.pbrm

3. 계산 실행

       user@Computer:~/$ pybaram run wedge.pbrm wedge.ini


4. VTK 형식으로 유동 해석 결과 변경

       user@Computer:~/$ pybaram export wedge.pbrm out-5000.pbrs out.vtu

5. Paraview를 이용한 유동 가시화
   - 저장된 Contour state로 가시화

         user@Computer:~/$ paraview --state=results/mach.pvsm

        ![Density Contour](./results/mach.png)

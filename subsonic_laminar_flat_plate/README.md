평판 주위 아음속 층류 유동 해석
============================================
평판에서 발달하는 층류 경계층 해석 

1. 유동 조건
   - $M=0.2$
   - x=0.3 일때 $Re_x=600000$
   - 또는 고도 8.5km 일 때 물성치 (plate_dim.ini 참고)

   ![Velocity Contour](./results/velocity_contour.png)    

2. pyBaram 양식으로 격자 변경
   - Triangular 격자인 mesh/plate_tri.msh 를 사용할 수 있다.
   
       user@Computer:~/$ pybaram import mesh/plate.msh plate.pbrm

3. 계산 실행

       user@Computer:~/$ pybaram run plate.pbrm plate.ini

4. VTK 형식으로 유동 해석 결과 변경

       user@Computer:~/$ pybaram export plate.pbrm out-50000.pbrs out.vtu

5. Paraview를 이용한 유동 가시화
   - 저장된 Contour state로 가시화
   - 차원이 있는 계산의 경우 (plate_dim.ini) 는 velocity2.pvsm 을 사용해야 한다.

         user@Computer:~/$ paraview --state=results/velocity.pvsm

    $x=0.2$ 일 때 Blasius 해와 비교
    ![Velocity Distribution](./results/velocity_distribution.png)



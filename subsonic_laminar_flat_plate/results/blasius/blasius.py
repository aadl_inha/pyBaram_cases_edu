from matplotlib import pylab as plt
from scipy.integrate import solve_bvp
import numpy as np


# Blasius equation: 2f''' + f*f'' = 0
def df(x, y):
    return np.vstack((y[1], y[2], -0.5*y[0]*y[2]))

# f(0) = 0, f'(0) = 0, f'(inf) = 1
def bc(ya, yb):
    return np.array([ya[0], ya[1], yb[1]-1])

# Make a storage 
eta = np.linspace(0, 10, 100)
f = np.zeros((3, eta.size))

# Solve BVP
blasius = solve_bvp(df, bc, eta, f)

# Plot velocity profile: f'
plt.plot(eta, blasius.sol(eta)[1])
plt.show()

arr = np.array([eta, blasius.sol(eta)[1]])
np.savetxt('blasius.csv', arr.T, delimiter=',', header='eta,u', comments='')

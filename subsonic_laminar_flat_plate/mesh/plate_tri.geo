Point(1) = {-0.05, 0, 0, 0.01};
Point(2) = {0, 0, 0, 0.01};
Point(3) = {0.3, 0, 0, 0.01};
Point(4) = {0.3, 0.03, 0, 0.01};
Point(5) = {0, 0.03, 0, 0.01};
Point(6) = {-0.05, 0.03, 0, 0.01};
//+
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 5};
Line(5) = {5, 6};
Line(6) = {1, 6};
Line(7) = {2, 5};
//+
Curve Loop(1) = {6, -5, -7, -1};
Plane Surface(1) = {1};
Curve Loop(2) = {4, -7, 2, 3};
Plane Surface(2) = {2};
//Recombine Surface {1, 2};
//+
Transfinite Curve {6, 7, 3} = 61 Using Progression 1.1;
Transfinite Curve {2, -4} = 37 Using Progression 1.1;
Transfinite Curve {-1 ,5} = 20 Using Progression 1.1;
Transfinite Surface {1, 2};
//+
Physical Curve("inflow", 8) = {6};
Physical Curve("outflow", 9) = {3, 4, 5};
Physical Curve("sym", 10) = {1};
Physical Curve("wall", 11) = {2};
Physical Surface("fluid", 12) = {1, 2};

